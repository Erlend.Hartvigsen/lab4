package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int columns;
    CellState[][] grid;
    



    public CellGrid(int rows, int columns, CellState initialState) {
        if(rows<=0) {
			throw new IndexOutOfBoundsException();
		}
		this.rows = rows;
		if(columns<=0) {
			throw new IndexOutOfBoundsException();
		}
        this.columns = columns;
        grid = new CellState [rows][columns];
        for(int row=0; row<rows; row++) {
			for(int column=0; column<columns; column++) {
				grid[row][column] = initialState;
			}
		}
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid copyGrid = new CellGrid(rows, columns, CellState.DEAD);
        for(int i=0; i<rows; i++){
            for(int j=0; j<columns; j++){
                CellState temp = this.get(i,j);
                copyGrid.set(i, j, temp);
            }
        }   
        return copyGrid;
    }
    
}
