package cellular;

import datastructure.IGrid;

public class BriansBrain extends GameOfLife {

    public BriansBrain(int rows, int columns) {
        super(rows, columns);
        //TODO Auto-generated constructor stub
    }

    @Override
    public CellState getNextCell(int row, int col) {
        // Levende blir døende
        if (getCellState(row, col) == CellState.ALIVE) {
            return CellState.DYING;
        }
        // Dødene blir død
        if (getCellState(row, col) == CellState.DYING) {
            return CellState.DEAD;
        }
        // Død blir levende hvis den har 2 levende naboer
        if (getCellState(row, col) == CellState.DEAD) {
            if (countNeighbors(row, col, CellState.ALIVE) == 2) {
                return CellState.ALIVE;
            }
            else return CellState.DEAD;
        }
        else return CellState.DEAD;
	}



}
